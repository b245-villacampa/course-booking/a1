const express = require("express");
const mongoose = require("mongoose");
// by default our backend's Cors setting will prevent any application outside our Express JS app to process the requesr. Using the cors package, it will allow us to manipulate this and control what applkication may use our app

// Allows our backend application to be available to our application.
// Allows us to control the app's Cross Origin Resources Sharing 
const cors = require("cors");

const userRoutes = require("./Routes/userRoutes.js");
const courseRoutes = require("./Routes/courseRoutes.js");

const port = 3001;
const app = express();

	//[mongoDB Connection]
	mongoose.connect("mongodb+srv://admin:admin@batch245-villacampa.g4r4ic2.mongodb.net/batch245_Course_API_Villacampa?retryWrites=true&w=majority", {
		useNewUrlParser:true,
		useUnifiedTopology:true
	})

	let db = mongoose.connection;

	// For error handling
	db.on("error", console.error.bind("Connection Error!"));

	// For validation 
	db.once("open", ()=>{console.log(`We are connected to the cloud!`)})



// middleWares
app.use(express.json());
app.use(express.urlencoded({extended:true}));
app.use(cors());

// Routing
app.use("/user", userRoutes);
app.use("/course", courseRoutes);

app.listen(port, ()=> console.log(`Server is running at ${port}`))

// model > controller > routes > server/application