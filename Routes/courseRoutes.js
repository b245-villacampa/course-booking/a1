const express = require("express");
const router = express.Router();
const courseController = require("../Controllers/courseController.js")
const auth = require("../auth.js")

// Routes for creating a course  
router.post("/", auth.verify, courseController.addCourse);

// Route for retrieving all courses
router.get("/all", auth.verify, courseController.allCourse);

// Routes for retrieving all active courses 
router.get("/allActive", courseController.allActiveCourses)

//Routes for retrieving inactive courses
router.get("/allInactive", auth.verify, courseController.allInactiveCourses);




// [Routes with Params]

// Route for retrieving detail/s of specific course
router.get("/:courseId", courseController.courseDetails);

router.put("/update/:courseId", auth.verify, courseController.updateCourse);

// Start of Activity code
	// Route for archiving
	router.put("/:courseId", auth.verify, courseController.archiveCourse);
// End of Activity code



module.exports = router;