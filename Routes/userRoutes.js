const express = require("express")
const router = express.Router();
const auth = require("../auth.js")
const userController = require("../Controllers/userController.js")

// Routes
	//This route is responsible for the registration of the user 
	router.post("/register", userController.userRegistration);

	// this route is for user authentication
	router.post("/login", userController.userAuthentication);

	// this route is to get the user document
	router.get("/details", auth.verify, userController.getProfile);

	
	router.post("/enroll/:courseId", auth.verify, userController.enrollCourse);
	 
module.exports = router;