const Course = require("../Models/coursesSchema.js");
const auth = require("../auth.js")


// Create a new course
/*
	Steps:
	1. Create a new Course object using the mongoose model and the information from the request body and the id from the header
	2. Save the new User to the database
*/

module.exports.addCourse = (request, response) =>{

		const userData = auth.decode(request.headers.authorization);
		if(userData.isAdmin){
			let input = request.body;

			let newCourse = new Course({
				name: input.name,
				description: input.description,
				price: input.price
			});

			//save the created object to our database 
			return newCourse.save().then(course=>{
				// Course successfully created
				
				response.send(course)
			}).catch(error => {
				
				response.send(false)
			})
		}else{

			return response.send("You're not an admin!")
		}
}

// Create a controller wherein it will retrieved all the courses(active/inactive courses)

module.exports.allCourse = (request, response) =>{
	const userData = auth.decode(request.headers.authorization);
	if(!userData.isAdmin){
		return response.send("You don't have access to this route!");
	}else{
		Course.find({}).then(result=>  response.send(result)).catch(error =>  response.send(error));
	}
}

// Create a controller wherein it will retrive course that are active

module.exports.allActiveCourses = (request, response) =>{
	Course.find({isActive:true}).then(result =>  response.send(result)).catch(error =>  response.send(error));
}

// Mini-Activity 
	//1.you are going to create a route wherein it can retrieve all inactive courses
	// Make sure that the admin users only are the ones can access this route

module.exports.allInactiveCourses=(request, response)=>{
	const userData = auth.decode(request.headers.authorization);

	if(!userData.isAdmin){
		return response.send("You don't have access");
	}else{
		Course.find({isActive:false}).then(result => response.send(result)).catch(error => response.send(error))
			
	}
}

// This controller will get the details of specified course
module.exports.courseDetails=(request,response)=>{
	const courseId = request.params.courseId;

	Course.findById(courseId).then(result => response.send(result)).catch(error => response.send(error))
}

// This controller is for updating specific course
/*
	Business Logic:
		1. We are going to edit/update the course that is stored in the params.

*/

module.exports.updateCourse= async(request, response)=>{

	const userData = auth.decode(request.headers.authorization);
	const courseId = request.params.courseId;
	const input = request.body;

	if(!userData.isAdmin){
		return response.send("You don't have an access in this page")
	}else{

		await Course.findById(courseId).then(result => {
			if(result === null){
				return response.send("CourseId is invalid, please try again!")
			}else{
				let updatedCourse = {
					name: input.name,
					description:input.description,
					price:input.price
				}

				Course.findByIdAndUpdate(courseId, updatedCourse, {new:true}).then(result => {
					
					return response.send(result)}).catch(error=>response.send(error));
			}
		}).catch(error=> response.send(error))
	}
}

// Start of Activity code - Archiving a course

module.exports.archiveCourse =  (request, response) =>{

	const userData = auth.decode(request.headers.authorization);
	const courseId = request.params.courseId;
	const input = request.body;

	if(!userData.isAdmin){
		return response.send("You don't have an access!");
	}else{
		Course.findById(courseId).then(result=>{
			if(result === null){
				return response.send("Course Id is not found!")
			}else{
				let archive = {
					isActive: input.isActive
				}
				Course.findByIdAndUpdate(courseId, archive, {new:true}).then(result=>response.send(result)).catch(error=>response.send(error))
			}
		})
	}
}
// End of Activity code - Archiving a course

