const mongoose = require("mongoose");
const User = require("../Models/usersSchema.js");
// bycrpt to encrypt the password
const bcrypt = require("bcrypt")
const auth = require("../auth.js")
const Course = require("../Models/coursesSchema.js");
// Controllers

// This controller will create or register a user on our db

module.exports.userRegistration = (request, response) =>{

	const input = request.body;

	User.findOne({email:input.email}).then(result =>{
		if(result!=null){
			return response.send("Email is already taken!");
		}else{
			let newUser = new User({
				firstName: input.firstName,
				lastName: input.lastName,
				email: input.email,
				password: bcrypt.hashSync(input.password, 10),
				mobileNo: input.mobileNo 
			})
			newUser.save().then(save=> {return response.send("You are now registered to our website!")}).catch(error=>{return response.send(error)});
		}
	})
}

// User Authentication
module.exports.userAuthentication = (request, response) =>{

	let input = request.body;

	// Possible scenarios in logging in 
		// 1. email is not yet registered
		// 2. email is registered but the password is wrong

	User.findOne({email:input.email}).then(result =>{
		if(result === null){
			return response.send("Email is not registered. Register first before logging in!")
		}else{
			// we have to verify if the password is correct

			// compareSync() (method) is used to compare non encrypted password to the encrypted password.
			// it returns bollean value, if match true value will return otherwise false.
			const isPasswordCorrect = bcrypt.compareSync(input.password, result.password)

			if(isPasswordCorrect){
				return response.send({auth: auth.createAccessToken(result)})
			}else{
				return response.send("Password is incorrect!");
			}
		}
	}).catch(error => {
		return response.send(error);
	})
}

module.exports.getProfile = (request, response) =>{

	// let input = request.body;
	const userData = auth.decode(request.headers.authorization);
	
	
	User.findById(userData._id).then(result =>{
		
			// avoid to expose sensitive information such as password
			result.password =" "; 
			return response.send(result);
		
		
	}).catch(error => {
		return response.send(error)})
}

// Controller for user enrollment
	//1.We can get the id of the user by decoding the jwt
	// 2. We can get the courseId by ucing the request params

module.exports.enrollCourse = async(request,response)=>{
	// First we have to get the userId and the courseId
	// decote the token to extract/unpack the payload

	const userData = auth.decode(request.headers.authorization);
	const courseId = request.params.courseId;

	// 2 things taht we need to do in this controller
		// First, to push the courseId in the enrollments property of the user
		// Second, to push the userId in the enrolles property of th course
	if(!userData.isAdmin){
		
		let isUserUpdated = await User.findById(userData._id).then(result => {
			if(result === null){
				return false;
			}

			else{
				result.enrollments.push({courseId: courseId});
				return result.save().then(save => true).catch(error => false);
			}
		})

		let isCourseUpdated = await Course.findById(courseId).then(result => {
			if(result === null){
				return false;
			}else{
				result.enrollees.push({userId: userData._id});
				return result.save().then(save => true).catch(error => false);
			}
		})

		if(isCourseUpdated && isUserUpdated){
			return response.send("The course is now enrolled!")
		}else{
			return response.send("There was an error during the enrollment. Please try again.")
		}
	}else{
		return response.send("You are an admin. You don't have an access to enroll!")
	}
}
