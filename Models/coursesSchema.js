const mongoose = require("mongoose");

const coursesSchema = new mongoose.Schema({
 	name:{
 		type: String,
 		required:[true, "Name of the course is required!"]
 	},
 	description:{
 		type: String,
 		required: [true, "description of the course is required"]
 	},
 	price:{
 		type: Number,
 		required: [true, "Price of the course is required!"]
 	},
 	isActive:{
 		type: Boolean,
 		default: true
 	},
 	createdOn:{
 		type: Date,
 		// The "new Date()" expression instantiates a new "date" taht stores the current date and time whenever a course is created in our database.
 	 default: new Date()
 	},
 	enrollees:[
 		{
 			userId:{
 				type:String,
 				required:[true, "UserId is required!"]
 			},
 			enrolledOn:{
 				type:Date,
 				deafult: new Date()
 			}
 		}
 	]
 });

 module.exports = mongoose.model("Course", coursesSchema);